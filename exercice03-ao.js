let http = require("http");
let server = http.createServer(function(request, response) {


'use strict';
/* 
on définit un Object litéral qui contient l'ensemble des provinces 
*/
let oProvince = {  "QC" : "Québec",
                   "ON" : "Ontario",
                   "MA" : "Manitoba",
                   "SK" : "Saskashewan",
                   "AL" : "Alberta",
                   "IPE" : "Île-du-Prince-Édouard",
                   "NB" : "Nouveau-Brunswick",
                   "NE" : "Nouvelle-Écosse",
                   "TNL" : "Terre-Neuve-et-Labrador",
                   "NU" : "Nunavut",
                   "TNO" : "Territoires du Nord-Ouest",
                   "YU" : "Yukon"
                }
/* 
permet d'extraire l'ensemble des propriétés valeurs de l'objet litéral */

const contenu_objet_json =(o)=>{


   let trace = '<table><tr><td><h3>Acronyme</h3></td><td><h3>Nom de la province</h3></td></tr>';
   for (let p in o) { 
     trace += '<tr><td>'+p + '</td><td>' + o[p] + '</td>'; 
   } 
   return trace+='</table>';
   }




response.writeHead(200, {"Content-Type": "text/html; charset=UTF-8"});
 response.write('<!DOCTYPE "html">');
 response.write('<html>');
 response.write('<head>');
 response.write('<title>Exercice 3 Aaron Orellana - Provinces et Territoires du Canada</title>');
 response.write('<h1>Exercice 3 Aaron Orellana - Provinces et Territoires du Canada</h1>');
 response.write('<style>table, th, td {border: 1px solid black;}</style>')
 response.write('</head>');
 response.write('<body>');
 response.write(contenu_objet_json(oProvince));
 response.write('</body>');
 response.write('</html>');
 response.end();

})
server.listen(3000)